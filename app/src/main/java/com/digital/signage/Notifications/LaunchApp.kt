package com.digital.signage.Notifications

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import com.digital.signage.Activity.WelcomeActivity

/**
 * Created by Vishal on 23/05/2018.
 */
class LaunchApp: BroadcastReceiver() {
    override fun onReceive(context: Context?, intent: Intent?) {

        openActivity(context!!)
    }

    private fun openActivity( context: Context) {
        val mIntent = Intent(context, WelcomeActivity::class.java)
        mIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
        mIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
        context.startActivity(mIntent)
    }
}