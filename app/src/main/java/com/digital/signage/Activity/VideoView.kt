package com.digital.signage.Activity

import android.support.v7.app.AppCompatActivity
import android.os.Bundle

import com.google.android.exoplayer2.trackselection.DefaultTrackSelector
import android.annotation.SuppressLint
import android.content.Context
import android.content.SharedPreferences
import android.net.Uri
import android.os.Environment
import android.os.PowerManager
import android.util.Log
import android.view.View
import android.widget.ImageView
import android.widget.ProgressBar
import android.widget.TextView
import android.widget.Toast
import com.digital.signage.R
import com.digital.signage.Utils.Utils
import com.digital.signage.custom.AutoScrollViewPager
import com.digital.signage.exoplayer.CacheDataSourceFactory
import com.digital.signage.exoplayer.ComponentListener
import com.digital.signage.exoplayer.DownloadVideo
import com.digital.signage.exoplayer.ImageVIdeoComponentListener
import com.digital.signage.retrofit.DataModel
import com.google.android.exoplayer2.*

import com.google.android.exoplayer2.source.MediaSource
import com.google.android.exoplayer2.extractor.DefaultExtractorsFactory
import com.google.android.exoplayer2.source.ExtractorMediaSource
import com.google.android.exoplayer2.util.Util
import kotlinx.android.synthetic.main.activity_video_view.*
import com.google.android.exoplayer2.source.ConcatenatingMediaSource
import com.google.android.exoplayer2.source.LoopingMediaSource
import com.google.android.exoplayer2.trackselection.AdaptiveTrackSelection
import com.google.android.exoplayer2.ui.SimpleExoPlayerView
import com.google.android.exoplayer2.upstream.*
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken

import kotlinx.android.synthetic.main.activity_video_view.*
import java.io.File
import java.util.ArrayList
import kotlin.jvm.internal.Ref


class VideoView : AppCompatActivity() {
    private var mTopTitleTxt: TextView? = null
    private var mBottomTxt: TextView? = null
    private var mEmptyScreenTxt: TextView? = null
    private var mPlayBool: Boolean? = false
    private var mWakeLock: PowerManager.WakeLock? = null
    companion object {
        public var errorImg: ImageView? = null
    }

    private var componentListener: ComponentListener? = null
    private val BANDWIDTH_METER = DefaultBandwidthMeter()
    // internal var videoview: SimpleExoPlayerView? = null
    internal var player: SimpleExoPlayer? = null
    internal var playWhenReady = true
    internal var playbackPosition = 0;
    var currentWindow = 0


    private val arrayList: ArrayList<Int>? = null

    private var mDataModel: DataModel? = null
    private val mDownload = DownloadVideo(this@VideoView)

    private var mPlayString: String? = null
    private var mPrefs: SharedPreferences? = null
    private var mProgress: ProgressBar? = null
    private val mStorageUrlList = ArrayList<String>()
    private var mTopTextStr: String? = null
    private var mVideoList: ArrayList<DataModel>? = null
    private var mediaSources: Array<MediaSource?>? = null
    private var mediaStorageDir: File? = null

    private var mExoPlayer: SimpleExoPlayerView? = null
    //  var mProgress:ProgressBar
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_video_view)



        var pm =  getSystemService(Context.POWER_SERVICE) as PowerManager
        mWakeLock = pm.newWakeLock(PowerManager.SCREEN_DIM_WAKE_LOCK, "My Tag");
        mWakeLock!!.acquire();
        mPrefs = getSharedPreferences("DigitalSignage", Context.MODE_PRIVATE)
        errorImg = findViewById<View>(R.id.img_connection_error) as ImageView

        mProgress = findViewById<View>(R.id.progress_bar) as ProgressBar
        componentListener = ComponentListener(mProgress!!)
        mTopTitleTxt = findViewById<View>(R.id.img_layout_bottomtxt) as TextView
        mBottomTxt = findViewById<View>(R.id.img_layout_bottomtxt) as TextView
        mEmptyScreenTxt = findViewById<View>(R.id.img_emty_screen_txt) as TextView

        mPlayBool=false
        mExoPlayer = findViewById<View>(R.id.video_view) as SimpleExoPlayerView
        mPlayString = mPrefs!!.getString("playTime", "")
        mTopTextStr = mPrefs!!.getString("TopText", "")
        var mDataModelStr = mPrefs!!.getString("DataModel", "")

        var mVideoListStr = mPrefs!!.getString("VideoModel", "")
        var gson = Gson()
        val type = object : TypeToken<DataModel>() {}.type
        mDataModel = gson.fromJson(mDataModelStr, type)

        val videoType = object : TypeToken<ArrayList<DataModel>>() {}.type
        mVideoList = gson.fromJson(mVideoListStr, videoType)
        checkInternet()
        checkVisibility()
    }

    /*Check if current time is ok to show the add else show message on screen */
    private fun checkVisibility() {
        mTopTitleTxt!!.text = mTopTextStr
        if (mPlayString == "Start") {
            mEmptyScreenTxt!!.visibility = View.GONE
            view1.visibility = View.VISIBLE
            mBottomTxt!!.visibility = View.VISIBLE

            mBottomTxt!!.isSelected = true
            mBottomTxt!!.text = mDataModel!!.text

            /*if Viddeo array size is zero then show empty screen else show viewpager*/
            if (mVideoList!!.isNotEmpty()) {
                mExoPlayer!!.visibility = View.VISIBLE
                mProgress!!.visibility = View.VISIBLE
                mEmptyScreenTxt!!.visibility = View.GONE

                initializePlayer()
                mPlayBool=true
            } else {
                mProgress!!.visibility = View.GONE
                mEmptyScreenTxt!!.visibility = View.VISIBLE
                mEmptyScreenTxt!!.text = "Silence is Golden!"
                mExoPlayer!!.visibility = View.GONE
            }


        } else {
            mProgress!!.visibility = View.GONE
            view1.visibility = View.GONE
            mExoPlayer!!.visibility = View.GONE
            mBottomTxt!!.visibility = View.GONE

            mEmptyScreenTxt!!.visibility = View.VISIBLE
            mEmptyScreenTxt!!.text = "Silence is Golden!"
        }
    }


    public override fun onStart() {
        super.onStart()
    }

    public override fun onResume() {
        super.onResume()
        hideSystemUi()
        if (player == null && mPlayBool!!) {
            initializePlayer()
        }
    }

    public override fun onPause() {
        super.onPause()
        if (player != null) {
            releasePlayer()
        }
    }

    public override fun onStop() {
        super.onStop()

        try {
            releasePlayer()
            mWakeLock!!.release()
        }catch (e:Exception){
            e.printStackTrace()
        }

    }
    /* private fun initializePlayer() {
         val adaptiveTrackSelectionFactory = AdaptiveTrackSelection.Factory(BANDWIDTH_METER)
         player = ExoPlayerFactory.newSimpleInstance(
                 DefaultRenderersFactory(this),
                 DefaultTrackSelector(adaptiveTrackSelectionFactory), DefaultLoadControl())

         video_view.setPlayer(player)
         player!!.addListener(componentListener);
         player!!.setVideoDebugListener(componentListener);
         player!!.setPlayWhenReady(playWhenReady)
         player!!.seekTo(currentWindow, playbackPosition.toLong())

       *//*  if(player!!.isLoading){
            progress_bar.visibility=View.GONE
        }else{
            progress_bar.visibility=View.VISIBLE
        }*//*
        val uri = Uri.parse(getString(R.string.media_url_mp4))
        val mediaSource = buildMediaSource(uri)
    //    val medi[]=
        player!!.prepare(mediaSource, true, true)
    }
*/
    /*private fun buildMediaSource(uri: Uri): MediaSource {
        // these are reused for both media sources we create below
        val extractorsFactory = DefaultExtractorsFactory()
        val dataSourceFactory = DefaultHttpDataSourceFactory("user-agent",BANDWIDTH_METER)

        val videoSource = ExtractorMediaSource(uri, dataSourceFactory,
                extractorsFactory, null, null)

        val audioUri = Uri.parse(getString(R.string.media_url_mp3))
        val audioSource = ExtractorMediaSource(audioUri, dataSourceFactory,
                extractorsFactory, null, null)
        val otherStrings = arrayOf("a", "b", "c")
        return ConcatenatingMediaSource(audioSource)
    }*/

    private fun initializePlayer() {
        val adaptiveTrackSelectionFactory = AdaptiveTrackSelection.Factory(BANDWIDTH_METER)
        player = ExoPlayerFactory.newSimpleInstance(
                DefaultRenderersFactory(this),
                DefaultTrackSelector(adaptiveTrackSelectionFactory), DefaultLoadControl())

        mExoPlayer!!.player = player
        player!!.addListener(componentListener);
        player!!.setVideoDebugListener(componentListener);
        player!!.playWhenReady = playWhenReady
        player!!.seekTo(currentWindow, playbackPosition.toLong())
        player!!.repeatMode = Player.REPEAT_MODE_ALL

        /*  if(player!!.isLoading){
              progress_bar.visibility=View.GONE
          }else{
              progress_bar.visibility=View.VISIBLE
          }*/
        val uri = Uri.parse(getString(R.string.media_url_mp4))
        val mediaSource = buildMediaSource(uri)
        //    val medi[]=
        player!!.prepare(mediaSource, true, true)
    }

    private fun buildMediaSource(uri: Uri): MediaSource {

        // these are reused for both media sources we create below
        val extractorsFactory = DefaultExtractorsFactory()
        val dataSourceFactory = DefaultHttpDataSourceFactory("DigitalSignage", BANDWIDTH_METER)
        val extPath = mPrefs!!.getString("downloadDirectory", "")
        /*check if sdcard path is exist or not if not then create in internal storage*/
        if (extPath == "") {
            mediaStorageDir = File(Environment.getExternalStorageDirectory(), "DigitalSignage/Videos")
        } else {
            mediaStorageDir = File(extPath.trim(), "DigitalSignage/Videos")
        }
        var file = mediaStorageDir
        if (!file!!.exists()) {
            file!!.mkdirs()
        }
        /*get saved videos list from storage*/
        var list = file.listFiles()

        for (i: Int in 0 until mVideoList!!.size) {
            var vidNameArr = mVideoList!![i].name.split("uploads/")
            var vidName = vidNameArr[1].trim()
            Log.e("vid Path::", File(mediaStorageDir, vidName).toString())
            /*if videos is stored in storage then show from storage else show via live streaming*/
            if (list.isNotEmpty()) {
                if ((list.contains(File(mediaStorageDir, vidName)) && list.size == mVideoList!!.size)) {

                    /*Show from Sdcard*/
                    if (i == 0) {
                        mediaSources = arrayOfNulls<MediaSource>(mVideoList!!.size)
                    }

                    val dataSpec = DataSpec(Uri.fromFile(list[i]))
                    val objectRef = Ref.ObjectRef<FileDataSource>()
                    objectRef.element = FileDataSource()
                    try {

                        (objectRef.element as FileDataSource).open(dataSpec)

                        mediaSources!![i] = ExtractorMediaSource(dataSpec.uri, DefaultDataSourceFactory(this@VideoView, "DigitalSignage"), extractorsFactory, null, null);

                    } catch (e: ExceptionInInitializerError) {

                    }

                } else {
                    /*Show online and download Videos*/
                    /*delete previous stored videos*/
                    if (list.isNotEmpty()) {
                        for (ii: Int in 0 until list.size) {
                            list[i].delete()
                        }
                    }
                    /*download videos in storage*/
                    if (i == 0) {
                        mediaSources = arrayOfNulls(mVideoList!!.size)
                        var mDownload = DownloadVideo(this@VideoView)
                        mDownload.downloadManager(mVideoList!!)
                    }

                    mediaSources!![i] = ExtractorMediaSource(Uri.parse(mVideoList!![i].name), DefaultDataSourceFactory(this, "DigitalSignage"), DefaultExtractorsFactory(), null, null)

                }
            } else {
                /*Show online and download Videos*/
                /*download videos in storage*/
                if (i == 0) {
                    mediaSources = arrayOfNulls(mVideoList!!.size)
                    var mDownload = DownloadVideo(this@VideoView)
                    mDownload.downloadManager(mVideoList!!)
                }

                mediaSources!![i] = ExtractorMediaSource(Uri.parse(mVideoList!![i].name), DefaultDataSourceFactory(this@VideoView, "DigitalSignage"), DefaultExtractorsFactory(), null, null)
            }

            /* val videoSource = ExtractorMediaSource(uri, dataSourceFactory,
                     extractorsFactory, null, null)

             val audioUri = Uri.parse(getString(R.string.media_url_mp3))
             val audioSource = ExtractorMediaSource(audioUri, dataSourceFactory,
                     extractorsFactory, null, null)
             val otherStrings = arrayOf("a", "b", "c")*/
        }
        if (mVideoList!!.size == 1)
            return LoopingMediaSource(mediaSources!![0])
        else
            return ConcatenatingMediaSource(*mediaSources!!);
        // return ConcatenatingMediaSource(*Arrays.copyOf<MediaSource>(mediaSources, mediaSources!!.size) as Array<MediaSource>);

    }

    @SuppressLint("InlinedApi")
    private fun hideSystemUi() {
        video_view.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LOW_PROFILE
                or View.SYSTEM_UI_FLAG_FULLSCREEN
                or View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                or View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
                or View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                or View.SYSTEM_UI_FLAG_HIDE_NAVIGATION)
    }

    private fun releasePlayer() {
        if (player != null) {
            playbackPosition = player!!.getCurrentPosition().toInt()
            player!!.removeListener(componentListener);
            currentWindow = player!!.getCurrentWindowIndex()
            playWhenReady = player!!.getPlayWhenReady()
            player!!.release()
            player = null
        }
    }

    fun checkInternet() {
        if (Utils(this@VideoView).isNetworkConnected()) {
            errorImg!!.visibility = View.GONE

        } else {
            errorImg!!.visibility = View.VISIBLE
            errorImg!!.setOnClickListener(View.OnClickListener {
                Toast.makeText(this, "Internet Not Connected!", Toast.LENGTH_SHORT).show()
            })
            Toast.makeText(this, "Please check internet connection!", Toast.LENGTH_SHORT).show()
        }
    }

}
