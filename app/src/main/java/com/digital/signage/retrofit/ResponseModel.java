package com.digital.signage.retrofit;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by Vishal on 23/05/2018.
 */

public class ResponseModel {
    @SerializedName("active")
    public String active;
    @SerializedName("data")
    public ArrayList<DataModel> dataList;
    @SerializedName("message")
    public String message;
    @SerializedName("status")
    public String status;
}
