package com.digital.signage.exoplayer;

import android.app.Activity;
import android.content.Context;


import com.digital.signage.R;
import com.google.android.exoplayer2.upstream.DataSource;
import com.google.android.exoplayer2.upstream.DataSource.Factory;
import com.google.android.exoplayer2.upstream.DefaultBandwidthMeter;
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory;
import com.google.android.exoplayer2.upstream.FileDataSource;
import com.google.android.exoplayer2.upstream.TransferListener;
import com.google.android.exoplayer2.upstream.cache.CacheDataSink;
import com.google.android.exoplayer2.upstream.cache.CacheDataSource;
import com.google.android.exoplayer2.upstream.cache.LeastRecentlyUsedCacheEvictor;
import com.google.android.exoplayer2.upstream.cache.SimpleCache;
import com.google.android.exoplayer2.util.Util;

import java.io.File;


import kotlin.jvm.internal.Intrinsics;


public final class CacheDataSourceFactory implements Factory {
    private Activity context;
    private final DefaultDataSourceFactory defaultDatasourceFactory;
    private final long maxCacheSize;
    private final long maxFileSize;

    public CacheDataSourceFactory( Activity context, long maxCacheSize, long maxFileSize) {
        Intrinsics.checkParameterIsNotNull(context, "context");
        this.context = context;
        this.maxCacheSize = maxCacheSize;
        this.maxFileSize = maxFileSize;
        String userAgent = Util.getUserAgent(this.context, this.context.getString(R.string.app_name));
        this.defaultDatasourceFactory = new DefaultDataSourceFactory((Context) this.context, (TransferListener) new DefaultBandwidthMeter(), (Factory) new DefaultDataSourceFactory(this.context, userAgent));
    }


    public DataSource createDataSource() {
        SimpleCache simpleCache = new SimpleCache(new File(this.context.getCacheDir(), "media"), new LeastRecentlyUsedCacheEvictor(this.maxCacheSize));
        return new CacheDataSource(simpleCache, this.defaultDatasourceFactory.createDataSource(), new FileDataSource(), new CacheDataSink(simpleCache, this.maxFileSize), 3, null);
    }
}
