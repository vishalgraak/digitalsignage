package com.digital.signage.Utils

import android.Manifest
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import android.support.v7.widget.Toolbar
import android.widget.TextView
import android.content.pm.PackageManager
import android.support.v4.content.PermissionChecker.checkCallingOrSelfPermission
import android.Manifest.permission
import android.Manifest.permission.ACCESS_FINE_LOCATION
import android.Manifest.permission.READ_CONTACTS
import android.app.ProgressDialog
import android.net.ConnectivityManager
import android.support.v4.app.ActivityCompat.requestPermissions
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import android.widget.EditText
import android.os.Build
import android.os.Environment
import android.support.v4.os.EnvironmentCompat


import android.support.v4.content.ContextCompat.getExternalFilesDirs
import android.util.Log


class Utils(internal var mActivity: Activity) {
    private var mProgress:ProgressDialog?=null;
    fun openActivity(webActivityClass: Class<*>) {
        val mIntent = Intent(mActivity, webActivityClass)
        mIntent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
        mActivity.startActivity(mIntent)
    }


    fun isEmailValid(email: String): Boolean {
        //TODO: Replace this with your own logic
        return email.contains("@")
    }

    fun isPasswordValid(password: String): Boolean {
        //TODO: Replace this with your own logic
        return password.length > 6
    }
    /*public void setToolbar(Toolbar toolbar Context m) {
        mActivity.setTitle("");
        toolbar = (Toolbar) mActivity.findViewById(R.id.toolbar_image);
        .setSupportActionBar(toolbar);
        mActivity.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        mActivity.getSupportActionBar().setDisplayShowHomeEnabled(true);
        TextView mTitle = (TextView) toolbar.findViewById(R.id.toolbar_title);
        mTitle.setText("Booked Tests");
        String title = mTitle.getText().toString();

    }*/

    fun getToast(msg: String) {
        //  Intrinsics.checkParameterIsNotNull(msg, NotificationCompat.CATEGORY_MESSAGE);
        Toast.makeText(mActivity, msg, Toast.LENGTH_SHORT).show()
    }

    fun showProgress() {
        mProgress = ProgressDialog(mActivity)

        if (mProgress != null) {
            mProgress!!.setMessage("Please Wait...")
            val progressDialog2 = mProgress

            mProgress!!.show()
        }

    }

    fun cancelProgress() {
        val progressDialog = mProgress
        if (progressDialog != null) {
            progressDialog!!.cancel()
        }

    }

     fun hideSoftKeyboard(input: EditText) {
        input.inputType = 0
        val imm = mActivity.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(input.windowToken, 0)
    }
    fun isNetworkConnected(): Boolean {
        val connectivityManager = mActivity.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val activeNetworkInfo = connectivityManager.activeNetworkInfo
        return activeNetworkInfo != null && activeNetworkInfo.isConnected
    }
    fun checkPermission(): Boolean {
        val permission = android.Manifest.permission.WRITE_EXTERNAL_STORAGE
        val res =mActivity.checkCallingOrSelfPermission(permission)
        return res == PackageManager.PERMISSION_GRANTED

    }

    fun checkWifiConnection(): Boolean {
        val connManager = mActivity.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val mWifi = connManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI)


        return mWifi.isConnected
    }


    fun requestPermission() {
        requestPermissions(mActivity,arrayOf<String>(Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.ACCESS_NETWORK_STATE),
                101)
    }

    /* returns external storage paths (directory of external memory card) as array of Strings */
    fun getExternalStorageDirectories(): Array<String?> {

        val results = ArrayList<String>()
try {
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) { //Method 1 for KitKat & above
        val externalDirs = getExternalFilesDirs(mActivity, null)

        for (file in externalDirs) {
            val path = file.path.split("/Android")[0]

            var addPath = false

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                addPath = Environment.isExternalStorageRemovable(file)
            } else {
                addPath = Environment.MEDIA_MOUNTED.equals(EnvironmentCompat.getStorageState(file))
            }

            if (addPath) {
                results.add(path)
            }
        }
    }

    if (results.isEmpty()) { //Method 2 for all versions
        // better variation of: http://stackoverflow.com/a/40123073/5002496
        var output = ""
        try {
            val process = ProcessBuilder().command("mount | grep /dev/block/vold")
                    .redirectErrorStream(true).start()
            process.waitFor()
            val `is` = process.inputStream
            val buffer = ByteArray(1024)
            while (`is`.read(buffer) !== -1) {
                output = output + String(buffer)
            }
            `is`.close()
        } catch (e: Exception) {
            e.printStackTrace()
        }

        if (!output.trim { it <= ' ' }.isEmpty()) {
            val devicePoints = output.split("\n".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
            for (voldPoint in devicePoints) {
                results.add(voldPoint.split(" ".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()[2])
            }
        }
    }

    //Below few lines is to remove paths which may not be external memory card, like OTG (feel free to comment them out)
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
        var i = 0
        while (i < results.size) {
            if (!results.get(i).toLowerCase().matches(".*[0-9a-f]{4}[-][0-9a-f]{4}".toRegex())) {
                Log.d("TAG", results.get(i) + " might not be extSDcard")
                results.removeAt(i--)
            }
            i++
        }
    } else {
        var i = 0
        while (i < results.size) {
            if (!results.get(i).toLowerCase().contains("ext") && !results.get(i).toLowerCase().contains("sdcard")) {
                Log.d("TAG2", results.get(i) + " might not be extSDcard")
                results.removeAt(i--)
            }
            i++
        }
    }
}catch (e:Exception){
    e.printStackTrace()
}
        val storageDirectories = arrayOfNulls<String>(results.size)
        for (i in results.indices) storageDirectories[i] = results.get(i)

        return storageDirectories
    }

}