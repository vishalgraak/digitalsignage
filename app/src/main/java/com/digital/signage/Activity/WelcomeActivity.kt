package com.digital.signage.Activity

import android.annotation.SuppressLint
import android.app.Dialog
import android.content.Intent
import android.content.SharedPreferences
import android.net.Uri
import android.os.AsyncTask
import android.os.Bundle
import android.support.customtabs.CustomTabsIntent
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.Toolbar
import android.text.TextUtils
import android.util.Log
import android.view.View
import android.widget.*
import com.digital.signage.Notifications.ScheduleAlarm
import com.digital.signage.R
import com.digital.signage.Utils.Utils
import com.digital.signage.fcm.MyFirebaseInstanceIDService
import com.digital.signage.retrofit.ApiClient
import com.digital.signage.retrofit.ApiInterface
import com.digital.signage.retrofit.DataModel
import com.digital.signage.retrofit.ResponseModel
import com.google.firebase.iid.FirebaseInstanceId
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.lang.Thread.sleep
import java.text.SimpleDateFormat
import java.util.*


/**
 * Created by Vishal on 23/05/2018.
 */
 public class WelcomeActivity : AppCompatActivity() {

    private var checkStartScreen = false
    private var curTimeBool = false

    private var day: String? = null
    private var dialog: Dialog? = null



    private var mAnothDayModel: DataModel? = null
    private var mCurrTimeModel: DataModel? = null
    private var mCurrDayModel: DataModel? = null
    private var mPrefs: SharedPreferences? = null
    private var mUtils: Utils? = null

    private var mfirewalImg: ImageView? = null


    private var nexTimeBool = java.lang.Boolean.valueOf(false)
    private var perTimeBool = java.lang.Boolean.valueOf(false)


    private var refreshedToken: String? = null


    private var mEndTime: String? = null

    private var startScreen: String? = null
private var scheduleAlarm:ScheduleAlarm?=null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_welcome)
        mUtils = Utils(this@WelcomeActivity)
        mPrefs = getSharedPreferences("DigitalSignage", 0)
        scheduleAlarm = ScheduleAlarm(this@WelcomeActivity)
        setToolbar()


        mCurrDayModel = DataModel()
        mAnothDayModel = DataModel()

        if (mPrefs!!.getString("regId", "").equals("")) {

            refreshedToken = FirebaseInstanceId.getInstance().token
            Log.e("get new token", "" + this.refreshedToken)
            if (refreshedToken != null) {
                val editor = mPrefs!!.edit()
                editor.putString("regId", this.refreshedToken)

                editor.apply()
            } else {
                startService(Intent(this, MyFirebaseInstanceIDService::class.java))

            }
        } else {
            Log.e("Token Stored", mPrefs!!.getString("regId", ""))
        }

        if (!mUtils!!.checkPermission()) {
            mUtils!!.requestPermission()
        }
        val gson = Gson()
        val mDataList = mPrefs!!.getString("DataModel", "")
       // showDialog()
        if(mDataList == "") {
            showDialog()
        }else{
            val type = object : TypeToken<DataModel>() {}.type
           val mDataModel:DataModel = gson.fromJson(mDataList, type)
            openActivity(mDataModel.type)
        }

    }

    override fun onResume() {
        super.onResume()
        if (dialog != null)
            if (!dialog!!.isShowing) {
                showDialog()
            }
    }

    @SuppressLint("ServiceCast")
    private fun showDialog() {
        dialog = Dialog(this)


        dialog!!.setContentView(R.layout.activity_custom_dialog)

        dialog!!.setTitle("Digital Signage")


        var signageIdEdit = dialog!!.findViewById<View>(R.id.signage_id_edit) as EditText


        val dialogButton = dialog!!.findViewById<View>(R.id.submit_btn) as Button
        dialogButton.setOnClickListener(View.OnClickListener {
            val signageId = signageIdEdit.text.toString()
            if (TextUtils.isEmpty(signageId)) {
                signageIdEdit.error = "This field is required"
            } else if (signageId.length != 12) {
                signageIdEdit.error = "Signage id is not correct"
            } else {
                mUtils!!.hideSoftKeyboard(signageIdEdit)
                if (mUtils!!.isNetworkConnected()) {
                    if (mPrefs!!.getString("regId", "").equals("")) {
                        refreshedToken = FirebaseInstanceId.getInstance().token
                        Log.e("get new token", "" + this.refreshedToken)
                        val editor = mPrefs!!.edit()
                        editor.putString("regId", this.refreshedToken)
                        editor.commit()
                        editor.apply()
                        Toast.makeText(this@WelcomeActivity, "Sync Error 108!", Toast.LENGTH_SHORT).show()

                    } else {
                        getData(signageId);
                        Log.e("Token Stored", mPrefs!!.getString("regId", ""))
                    }
                } else {
                    Toast.makeText(this@WelcomeActivity, "Please check internet connection!", Toast.LENGTH_SHORT).show()
                }
            }
        })

        dialog!!.show()
    }

    private fun getData(signageId: String) {
        mUtils!!.showProgress()
        val regId = mPrefs!!.getString("regId", "")
        val apiService = ApiClient.getClient()!!.create(ApiInterface::class.java!!)
        try {
            val call = apiService.getSignageData("get_add_data", signageId, regId)
            call.enqueue(object : Callback<ResponseModel> {
                override fun onResponse(call: Call<ResponseModel>, response: Response<ResponseModel>) {
                    dialog!!.dismiss()
                    var responseData: ResponseModel = response.body()
                    var status = responseData.status
                    var message = responseData.message
                    var signageActive = responseData.active
                    if (status == "true") {
                        if (signageActive == "true") {
                            var mDataList = responseData.dataList

                            AsyncAlarmScheduler().execute(mDataList)
                            var mEditor = mPrefs!!.edit()
                            mEditor.putString("SignageId", signageId)
                            mEditor.putString("active", "true")
                            mEditor.apply()
                        } else if (signageActive.equals("false")) {
                            var mEditor = mPrefs!!.edit()
                            mEditor.putString("SignageId", signageId)
                            mEditor.putString("active", "false")
                            mEditor.apply()
                            mUtils!!.openActivity(ImagePagerView::class.java)
                            finish()
                        }

                    } else {
                        mUtils!!.cancelProgress()
                        Toast.makeText(this@WelcomeActivity, message, Toast.LENGTH_SHORT).show()
                        if (!dialog!!.isShowing) {
                            showDialog()
                        }
                        if (signageActive.equals("false")) {
                            var mEditor = mPrefs!!.edit()
                            mEditor.putString("SignageId", signageId)
                            mEditor.putString("active", "false")
                            mEditor.apply()
                            mUtils!!.openActivity(ImagePagerView::class.java)
                            finish()
                        }
                    }


                }

                override fun onFailure(call: Call<ResponseModel>, t: Throwable) {
                    // Log error here since request failed
                    try {
                        mUtils!!.cancelProgress()
                        if (!dialog!!.isShowing) {
                            showDialog()
                        }
                        /*Check for thr firewall proxy*/
                        if (mUtils!!.checkWifiConnection()) {
                            Toast.makeText(this@WelcomeActivity, "Internet not connected.To configure firewall please click on top right icon.", Toast.LENGTH_LONG).show()
                            mfirewalImg!!.visibility = View.VISIBLE
                        } else {
                            mfirewalImg!!.visibility = View.GONE
                        }
                        Log.e("ERRRRRRRREEEERRRR", t.toString())
                    } catch (e: IllegalStateException) {
                        e.printStackTrace()
                    }
                }
            })
        } catch (e: IllegalStateException) {
            e.printStackTrace()
        }
    }


    inner class AsyncAlarmScheduler : AsyncTask<ArrayList<DataModel>, String, String>() {



        override fun doInBackground(vararg params: ArrayList<DataModel>?):String {
            //To change body of created functions use File | Settings | File Templates.
            checkStartScreen = false
            setScheduleAlarm(params[0]!!)

            return ""
        }

        override fun onPostExecute(result: String?) {
            super.onPostExecute(result)
            setScheduleScreen()
        }

    }



    fun setScheduleScreen() {
        try {



           // scheduleAlarm.scheduleAlarm(this@WelcomeActivity, 18, 12, mDataList[0], "sun", "Start")
            //scheduleAlarm.scheduleAlarm(this@WelcomeActivity, 18, 13, mDataList[1], "sun", "Start")
            mUtils!!.cancelProgress()
            if (checkStartScreen) {
                if (curTimeBool) {
                    curTimeBool=false
                    startScreen = "Start"
                    showScreen(mCurrTimeModel!!)
                } else {
                    startScreen = "Stop"
                    showScreen(mCurrDayModel!!)
                }

            } else {
                Toast.makeText(this, "No add for current day.", Toast.LENGTH_SHORT).show()
                startScreen = "Stop"
                showScreen(mAnothDayModel!!)
            }
            if (dialog!!.isShowing) {
                dialog!!.dismiss()
            }


        } catch (e: Exception) {
            e.printStackTrace()
            mUtils!!.cancelProgress()
            if (!dialog!!.isShowing) {
               showDialog()
            }
                   }
    }

    public fun setScheduleAlarm(mDataList: ArrayList<DataModel>){



        for (a: Int in 0 until mDataList.size) {
            val mTimingList = mDataList[a].timing
            if (mTimingList.isNotEmpty()) {
                for (i in 0 until mTimingList.size) {
                    day = mTimingList[i].day
                    var time = mTimingList[i].time
                    if (time == "-1") {


                        scheduleAlarm!!.scheduleAlarm(this@WelcomeActivity, 0, 0, mDataList[a], day, "Start")
                    } else {
                        val mScheduleList = mTimingList[i].timeList

                        for (c in 0 until mScheduleList.size) {
                            val str: String
                            val scheduleTime = mScheduleList[c].schedule

                            val strSplit = scheduleTime.split("-")
                            val startTime = strSplit[0].trim()
                            val startHour = startTime.split(":")[0].trim()
                            val startMinutes = startTime.split(":")[1].trim()
                            val endTime = strSplit[1].trim()
                            val endHour = endTime.split(":")[0].trim()
                            val endMinutes = endTime.split(":")[1].trim()
                            scheduleAlarm!!.scheduleAlarm(this@WelcomeActivity, startHour.toInt(), startMinutes.toInt(), mDataList[a], day, "Start")
                            /* if (mScheduleList.size == 1) {
                                 scheduleAlarm.scheduleAlarm(this@WelcomeActivity, startHour.toInt(), startMinutes.toInt(), mDataList[a], day, "Start")
                                 scheduleAlarm.scheduleAlarm(this@WelcomeActivity, endHour.toInt(), endMinutes.toInt(), mDataList[a], day, "Stop")
                             } else {

                                 if (startTime == mEndTime) {
                                     scheduleAlarm.scheduleAlarm(this@WelcomeActivity, startHour.toInt(), startMinutes.toInt(), mDataList[a], day, "Start")

                                 } else {
                                     scheduleAlarm.scheduleAlarm(this@WelcomeActivity, startHour.toInt(), startMinutes.toInt(), mDataList[a], day, "Start")
                                     scheduleAlarm.scheduleAlarm(this@WelcomeActivity, endHour.toInt(), endMinutes.toInt(), mDataList[a], day, "Stop")
                                 }
                             }*/
                            mEndTime = endTime
                            val dayOfTheWeek = SimpleDateFormat("EEE").format(Date())
                            var currentDay = day!!.substring(0, 1).toUpperCase() + day!!.substring(1, day!!.length).toLowerCase()
                            Log.e("daycheck",dayOfTheWeek+",,"+currentDay)
                            if (dayOfTheWeek == currentDay) {

                                checkStartScreen = true
                                if (schedulingFirstScreen(startTime, endTime)) {
                                    Log.e("current", ""+scheduleTime)
                                    mCurrTimeModel = mDataList[a]
                                } else {
                                    Log.e("not current", ""+scheduleTime)
                                    mCurrDayModel = mDataList[a]
                                }
                            } else {

                                mAnothDayModel = mDataList[a]
                            }

                        }
                    }


                }
            }
        }
    }
    private fun showScreen(mDataModel: DataModel) {
        val gson = Gson()
        val mDataModelstr = gson.toJson(mDataModel)
        val mImageModelstr = gson.toJson(mDataModel.imagesList)
        val mVideoModelstr = gson.toJson(mDataModel!!.videos)


        val mEditor = mPrefs!!.edit()
        mEditor.putString("DataModel", mDataModelstr)
        mEditor.putString("ImageModel", mImageModelstr)
        mEditor.putString("VideoModel", mVideoModelstr)
        mEditor.putString("active", "true")
        mEditor.putString("playTime", startScreen)
        mEditor.putString("TopText", mDataModel.topText)
        try {
            val downloadDir = mUtils!!.getExternalStorageDirectories()

            for (i in 0 until downloadDir.size) {
                mEditor.putString("downloadDirectory", downloadDir[i])
            }
            mEditor.apply()
        } catch (e: Exception) {
            e.printStackTrace()
        }
        mUtils!!.cancelProgress()
        val screenType = mDataModel.type

        openActivity(screenType)
    }

    private fun openActivity(screenType: String?) {

        val mIntent: Intent
        if (screenType == "img") {
            mIntent = Intent(this, ImagePagerView::class.java)
            mIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
            startActivity(mIntent)
            finish()
        } else if (screenType == "video") {
            mIntent = Intent(this, VideoView::class.java)
            mIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
            startActivity(mIntent)
            finish()
        } else {
            mIntent = Intent(this, ImageVideoView::class.java)
            mIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
            startActivity(mIntent)
            finish()
        }
    }

    @SuppressLint("LongLogTag")
    private fun schedulingFirstScreen(startTime: String, endTime: String): Boolean {
        val cal = Calendar.getInstance()
        val mFormat = SimpleDateFormat("HH:mm")
        val d1 = mFormat.parse(startTime)

        cal.time = d1
        val mStart = cal.timeInMillis
        cal.clear()
        if(endTime.trim() == "00:00"){
            cal.time = mFormat.parse("23:59")
        }else{
            cal.time = mFormat.parse(endTime)
        }
        val mEnd = cal.timeInMillis
        val result = mFormat.format(Calendar.getInstance().time)
        cal.clear()
        cal.time = mFormat.parse(result)
        val mReal = cal.timeInMillis
        val j = mEnd - 1
        if (mStart <= mReal && j >= mReal) {
            Log.e("Current Start Time and End Time", "$startTime=$endTime=$result")
            curTimeBool = true
           return true
        } else if (mReal > mStart) {
            Log.e("Previous Start Time and End Time", "$startTime=$endTime=$result")
            perTimeBool = true
            return false
        } else {
            Log.e("next Start Time and End Time", "" + startTime + '=' + endTime)
            nexTimeBool = true
            return false

        }

    }

    @SuppressLint("ResourceAsColor")
    private fun setToolbar() {
        val toolbar = findViewById<View>(R.id.toolbar_image) as Toolbar
        setSupportActionBar(toolbar)
        toolbar.setTitle("")
        val mTitle = toolbar.findViewById<View>(R.id.toolbar_title) as TextView
        mTitle.setText("Digital Signage")

        mfirewalImg = toolbar.findViewById<View>(R.id.ivImage) as ImageView





        mfirewalImg!!.setVisibility(View.GONE)

        mfirewalImg!!.setOnClickListener(View.OnClickListener {
            val builder = CustomTabsIntent.Builder()
            val customTabsIntent = builder.build()
            customTabsIntent.launchUrl(this, Uri.parse("http://atechindia.com"))
        })
    }


}