package com.digital.signage.Activity

import android.content.Context
import android.content.SharedPreferences
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v4.view.ViewPager
import android.view.MotionEvent
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import com.digital.signage.R
import com.digital.signage.Utils.Utils
import com.digital.signage.custom.AutoScrollViewPager
import com.digital.signage.retrofit.DataModel
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken

import kotlinx.android.synthetic.main.activity_image_view.*

import android.os.PowerManager
import java.util.*


class ImagePagerView : AppCompatActivity() {
    companion object {
        public var errorImg: ImageView? = null
    }

    private var mDataModel: DataModel? = null
    private var mTopTitleTxt: TextView? = null
    private var mBottomTxt: TextView? = null
    private var mEmptyScreenTxt: TextView? = null
    private var mWakeLock: PowerManager.WakeLock? = null
    private var mImageList: ArrayList<DataModel>? = null
    private var mPlayString: String? = null
    private var mPrefs: SharedPreferences? = null
    private var mTopTextStr: String? = null
    private var viewPager: AutoScrollViewPager? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_image_view)

        mPrefs = getSharedPreferences("DigitalSignage", Context.MODE_PRIVATE)
        errorImg = findViewById<View>(R.id.img_connection_error) as ImageView

        var pm =  getSystemService(Context.POWER_SERVICE) as PowerManager
        mWakeLock = pm.newWakeLock(PowerManager.SCREEN_DIM_WAKE_LOCK, "My Tag");
        mWakeLock!!.acquire();

        val active = mPrefs!!.getString("active", "")
        checkInternet()
        mTopTitleTxt = findViewById<View>(R.id.img_layout_top_title_txt) as TextView
        mBottomTxt = findViewById<View>(R.id.img_layout_bottomtxt) as TextView
        mEmptyScreenTxt = findViewById<View>(R.id.img_emty_screen_txt) as TextView
        viewPager = findViewById<View>(R.id.img_layout_title_pager) as AutoScrollViewPager
        /*check if signage is active or not if inactive then show only message*/
        if (active == "false") {
            view1.visibility = View.GONE
            viewPager!!.visibility = View.GONE
            mBottomTxt!!.visibility = View.GONE
            mEmptyScreenTxt!!.visibility = View.VISIBLE
            mEmptyScreenTxt!!.text = "Signage Inactive!"
            mTopTitleTxt!!.text = "Advance Tech India"
        } else {
            mPlayString = mPrefs!!.getString("playTime", "")
            mTopTextStr = mPrefs!!.getString("TopText", "")
            var mDataModelStr = mPrefs!!.getString("DataModel", "")
            var mImageListStr = mPrefs!!.getString("ImageModel", "")
            var gson = Gson()
            val type = object : TypeToken<DataModel>() {}.type
            mDataModel = gson.fromJson(mDataModelStr, type)
            val listType = object : TypeToken<ArrayList<DataModel>>() {}.type
            mImageList = gson.fromJson(mImageListStr, listType)
            checkVisibility()
        }


        //  val indicator = findViewById(R.id.indicator) as me.relex.circleindicator.CircleIndicator
        // indicator.setViewPager(viewPager)
        // viewPager.adapter.registerDataSetObserver(indicator.getDataSetObserver())

    }


    /*Check if current time is ok to show the add else show message on screen */
    private fun checkVisibility() {
        mTopTitleTxt!!.text = mTopTextStr
        if (mPlayString == "Start") {
            view1.visibility = View.VISIBLE
            mBottomTxt!!.visibility = View.VISIBLE
            mBottomTxt!!.isSelected = true
            mBottomTxt!!.text = mDataModel!!.text
            val stringBuilder = StringBuilder()
            val speed = mDataModel!!.speed
            val strInterval = stringBuilder.append(speed).append("000").toString()
            viewPager!!.interval = strInterval.toLong()
            viewPager!!.isCycle = true
            viewPager!!.startAutoScroll()
            viewPager!!.isStopScrollWhenTouch = true

            /*if image array size is zero then show empty screen else show viewpager*/
            if (mImageList!!.isNotEmpty()) {
                mEmptyScreenTxt!!.visibility = View.GONE
                viewPager!!.visibility = View.VISIBLE
                var adapter = CustomPagerAdapter(this,mImageList!! )
                viewPager!!.adapter = adapter
            } else {
                mEmptyScreenTxt!!.visibility = View.VISIBLE
                mEmptyScreenTxt!!.text = "Silence is Golden!"
                viewPager!!.visibility = View.GONE
            }
        } else {
            view1.visibility = View.GONE
            viewPager!!.visibility = View.GONE
            mBottomTxt!!.visibility = View.GONE
            mEmptyScreenTxt!!.visibility = View.VISIBLE
            mEmptyScreenTxt!!.text = "Silence is Golden!"
        }
        viewPager!!.setOnTouchListener(View.OnTouchListener() { view: View, motionEvent: MotionEvent ->
            return@OnTouchListener true
        });
    }

    fun checkInternet() {
        if (Utils(this@ImagePagerView).isNetworkConnected()) {
           errorImg!!.visibility = View.GONE

        } else {
           errorImg!!.visibility = View.VISIBLE
           errorImg!!.setOnClickListener(View.OnClickListener {
                Toast.makeText(this, "Internet Not Connected!", Toast.LENGTH_SHORT).show()
            })
            Toast.makeText(this, "Please check internet connection!", Toast.LENGTH_SHORT).show()
        }
    }

    public override fun onStop() {
        super.onStop()

        try {

            mWakeLock!!.release()
        }catch (e:Exception){
            e.printStackTrace()
        }

    }
}
