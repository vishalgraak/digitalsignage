package com.digital.signage.Notifications

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import com.digital.signage.retrofit.DataModel
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import android.net.ConnectivityManager
import android.util.Log
import android.view.View
import com.digital.signage.Activity.ImagePagerView
import com.digital.signage.Activity.ImagePagerView.Companion.errorImg
import com.digital.signage.Activity.ImageVideoView
import com.digital.signage.Activity.VideoView


/**
 * Created by Vishal on 23/05/2018.
 */
class WifiReciever : BroadcastReceiver() {
    private var mPrefs: SharedPreferences? = null
    private var mIsConnected: Boolean? = true
    override fun onReceive(context: Context?, intent: Intent?) {
        try {
            mPrefs = context!!.getSharedPreferences("DigitalSignage", Context.MODE_PRIVATE)
            var mDataModelStr: String = mPrefs!!.getString("DataModel", "")
            val gson = Gson()
            val type = object : TypeToken<DataModel>() {}.type
            var mDtataModel: DataModel = gson.fromJson(mDataModelStr, type)
            var screenType = mDtataModel.type
            val connManager = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
            val activeNetworkInfo = connManager.activeNetworkInfo
            // if network is not  connected the show network connection image
            if (activeNetworkInfo != null && activeNetworkInfo.isConnected) {

                Log.e("WifiReceiver", "have Wifi Connection")
                mIsConnected = true
                connectionError(context, screenType)
            } else {
                mIsConnected = false
                connectionError(context, screenType)
                Log.e("WifiReceiver", "Don't have Wifi Connection")
            }
        }catch (e:Exception){
            e.printStackTrace()
        }
    }

    private fun connectionError(context: Context, screenType: String) {
        if (screenType == "img") {
            if (mIsConnected!!) {
                ImagePagerView.errorImg!!.visibility = View.GONE
            } else {
                ImagePagerView.errorImg!!.visibility = View.VISIBLE
            }
        } else if (screenType == "video") {
            if (mIsConnected!!) {
                VideoView.errorImg!!.visibility = View.GONE
            } else {
                VideoView.errorImg!!.visibility = View.VISIBLE
            }
        } else {
            if (mIsConnected!!) {
               ImageVideoView.errorImg!!.visibility = View.GONE
            } else {
                ImageVideoView.errorImg!!.visibility = View.VISIBLE
            }
        }
    }
}