package com.digital.signage.retrofit;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by Vishal on 23/05/2018.
 */


public class DataModel implements Parcelable {
    @SerializedName("add_id")
    public String addId;
    @SerializedName("day")
    public String day;
    @SerializedName("images")
    public ArrayList<DataModel> imagesList;
    @SerializedName("name")
    public String name;
    @SerializedName("schedule")
    public String schedule;
    @SerializedName("speed")
    public String speed;
    @SerializedName("text")
    public String text;
    @SerializedName("time")
    public String time;
    @SerializedName("timelist")
    public ArrayList<DataModel> timeList;
    @SerializedName("timing")
    public ArrayList<DataModel> timing;
    @SerializedName("top_text")
    public String topText;
    @SerializedName("type")
    public String type;
    @SerializedName("videos")
    public ArrayList<DataModel> videos;
    public DataModel() {

    }
    protected DataModel(Parcel in) {
        addId = in.readString();
        day = in.readString();
        imagesList = in.createTypedArrayList(DataModel.CREATOR);
        name = in.readString();
        schedule = in.readString();
        speed = in.readString();
        text = in.readString();
        time = in.readString();
        timeList = in.createTypedArrayList(DataModel.CREATOR);
        timing = in.createTypedArrayList(DataModel.CREATOR);
        topText = in.readString();
        type = in.readString();
        videos = in.createTypedArrayList(DataModel.CREATOR);
    }

    public static final Creator<DataModel> CREATOR = new Creator<DataModel>() {
        @Override
        public DataModel createFromParcel(Parcel in) {
            return new DataModel(in);
        }

        @Override
        public DataModel[] newArray(int size) {
            return new DataModel[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(addId);
        dest.writeString(day);
        dest.writeTypedList(imagesList);
        dest.writeString(name);
        dest.writeString(schedule);
        dest.writeString(speed);
        dest.writeString(text);
        dest.writeString(time);
        dest.writeTypedList(timeList);
        dest.writeTypedList(timing);
        dest.writeString(topText);
        dest.writeString(type);
        dest.writeTypedList(videos);
    }
}
