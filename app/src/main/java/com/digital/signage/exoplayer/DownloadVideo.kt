package com.digital.signage.exoplayer

import android.app.Activity
import android.app.DownloadManager
import android.content.Context
import android.net.Uri
import android.os.Environment
import android.content.Context.DOWNLOAD_SERVICE
import android.util.Log
import android.view.View
import com.digital.signage.Utils.Utils
import com.digital.signage.retrofit.DataModel

import okhttp3.internal.Util
import java.io.File


/**
 * Created by Vishal on 23/05/2018.
 */
 public class DownloadVideo(var context: Activity){
    private var downloadManager:DownloadManager?=null
    private var mediaStorageDir: File? = null
    private var mUtils: Utils?=null
     fun downloadManager(videos: ArrayList<DataModel>){
        mUtils=Utils(context)
for(i:Int in 0 until videos.size){

    downloadRequest(videos,i,videos[i].name)
}
    }

    private fun downloadRequest(videosList:ArrayList<DataModel>,i:Int,vidUrl:String){
        // Create request for android download manager
        var vidNameArr= videosList!![i].name.split("uploads/")
        var vidName=vidNameArr[1].trim()
        Log.e("vid Pathdown",vidName)
        downloadManager = context.getSystemService(DOWNLOAD_SERVICE) as DownloadManager
        val request = DownloadManager.Request(Uri.parse(vidUrl))
        val mPrefs = context.getSharedPreferences("DigitalSignage", 0)
        request.setTitle(vidName)
        request.setAllowedOverRoaming(true);
        request.allowScanningByMediaScanner();
        request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);
        val extPath = mPrefs.getString("downloadDirectory", "").trim()
        val extList = mUtils!!.getExternalStorageDirectories()

       if(null!=extPath && !extPath.equals("")){
           mediaStorageDir = File(extPath,"DigitalSignage/Videos")
       }else if(extList.isNotEmpty()){
           mediaStorageDir = File(extList[0]!!.trim(),"DigitalSignage/Videos")
       }else{
           mediaStorageDir = File(Environment.getExternalStorageDirectory(), "DigitalSignage/Videos")
       }

        if (!mediaStorageDir!!.exists())
            mediaStorageDir!!.mkdir()
        Log.e("media Directory::",File(mediaStorageDir, vidName).toString())
        //Set the local destination for the downloaded file to a path
        //within the application's external files directory

        request.setDestinationUri(Uri.withAppendedPath(Uri.fromFile(mediaStorageDir), vidName))


        //Enqueue download and save into referenceId
        downloadManager!!.enqueue(request)
    }
}