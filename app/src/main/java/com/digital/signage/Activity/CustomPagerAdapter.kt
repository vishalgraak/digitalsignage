package com.digital.signage.Activity

import android.app.Activity
import android.support.v4.view.PagerAdapter
import android.view.View
import android.view.ViewGroup
import android.view.LayoutInflater
import android.widget.ImageView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.RequestOptions
import com.bumptech.glide.request.target.Target
import com.digital.signage.R
import com.digital.signage.retrofit.DataModel
import kotlinx.android.synthetic.main.activity_pager_adapter.view.*
import kotlinx.android.synthetic.main.activity_video_view.*
import java.util.*


class CustomPagerAdapter(var imagePagerView: Activity, var arrayList: ArrayList<DataModel>) : PagerAdapter() {

    override fun isViewFromObject(view: View, `object`: Any): Boolean {

        return view === `object` as View
    }

     override fun instantiateItem(container: ViewGroup, position: Int): Any {
         val inflater = LayoutInflater.from(imagePagerView)
         val layout = inflater.inflate(R.layout.activity_pager_adapter, container, false) as ViewGroup
         container.addView(layout)
         // mImageView = layout!!.findViewById<View>(R.id.pager_image_adapter) as ImageView
       //  layout.pager_image_adapter.setImageResource(arrayList!!.get(position))
         loadImageFromDiskCache(arrayList!![position].name,layout.pager_image_adapter)
         hideSystemUi(layout.pager_image_adapter)
         return layout

     }

    override fun getCount(): Int {
       return arrayList!!.size
    }

    override fun destroyItem(collection: ViewGroup, position: Int, view: Any) {
        collection.removeView(view as View)
    }

     private fun loadImageFromDiskCache(strUrl: String, tileView: ImageView) {
         var requestOptions =  RequestOptions();
         requestOptions.diskCacheStrategy(DiskCacheStrategy.RESOURCE)
         Glide.with(imagePagerView)
                 .load(strUrl)
                 .apply(requestOptions)
                 .into(tileView);
     }
    private fun hideSystemUi(imageView: ImageView) {
        imageView.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LOW_PROFILE
                or View.SYSTEM_UI_FLAG_FULLSCREEN
                or View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                or View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
                or View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                or View.SYSTEM_UI_FLAG_HIDE_NAVIGATION)
    }
}


