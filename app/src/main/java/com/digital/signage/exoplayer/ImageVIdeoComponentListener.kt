package com.digital.signage.exoplayer

import android.view.Surface
import android.view.View
import android.widget.ProgressBar


import com.google.android.exoplayer2.*
import com.google.android.exoplayer2.decoder.DecoderCounters
import com.google.android.exoplayer2.source.TrackGroupArray
import com.google.android.exoplayer2.trackselection.TrackSelectionArray
import com.google.android.exoplayer2.video.VideoRendererEventListener

class ImageVIdeoComponentListener(var mProgress: ProgressBar) : ExoPlayer.EventListener, VideoRendererEventListener {
    override fun onSeekProcessed() {
      //  TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onPositionDiscontinuity(reason: Int) {
       // TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onRepeatModeChanged(repeatMode: Int) {
      //  TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onShuffleModeEnabledChanged(shuffleModeEnabled: Boolean) {
      //  TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onVideoEnabled(counters: DecoderCounters?) {
         // TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
     }

     override fun onVideoSizeChanged(width: Int, height: Int, unappliedRotationDegrees: Int, pixelWidthHeightRatio: Float) {
         // TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
     }

     override fun onVideoDisabled(counters: DecoderCounters?) {
         //  TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
     }

     override fun onVideoDecoderInitialized(decoderName: String?, initializedTimestampMs: Long, initializationDurationMs: Long) {
         //  TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
     }

     override fun onVideoInputFormatChanged(format: Format?) {
         //  TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
     }

     override fun onRenderedFirstFrame(surface: Surface?) {
         // TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
     }

     override fun onDroppedFrames(count: Int, elapsedMs: Long) {
         //  TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
     }

     override fun onTracksChanged(trackGroups: TrackGroupArray?, trackSelections: TrackSelectionArray?) {
         // TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
     }

     override fun onPlayerError(error: ExoPlaybackException?) {
         // TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
     }

     override fun onPlayerStateChanged(playWhenReady: Boolean, playbackState: Int) {
         //  playWhenReady ==true
         val stateString: String
         when (playbackState) {
             ExoPlayer.STATE_IDLE ->stateString = "ExoPlayer.STATE_IDLe     -"
         /* if (!playWhenReady){
              mProgress.visibility=View.VISIBLE
          }    else {
              mProgress.visibility=View.GONE
          }*/
         //   mProgress.visibility=View.VISIBLE
             ExoPlayer.STATE_BUFFERING -> mProgress.visibility= View.VISIBLE

         /* if (!playWhenReady){
              mProgress.visibility=View.VISIBLE
          }    else {
              mProgress.visibility=View.GONE
          }*/
             ExoPlayer.STATE_READY ->/* stateString = "ExoPlayer.STATE_READY     -"*/
                 mProgress.visibility= View.GONE
         /* if (!playWhenReady){

          }    else {
              mProgress.visibility=View.GONE
          }*/
             ExoPlayer.STATE_ENDED -> stateString = "ExoPlayer.STATE_ENDED     -"
             else -> stateString = "UNKNOWN_STATE             -"
         }
         /* Log.d(TAG, "changed state to " + stateString
                  + " playWhenReady: " + playWhenReady);*/
     }

     override fun onLoadingChanged(isLoading: Boolean) {
         //TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
     }


     override fun onTimelineChanged(timeline: Timeline?, manifest: Any?) {
         // TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
     }

     override fun onPlaybackParametersChanged(playbackParameters: PlaybackParameters?) {
         // TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
     }

 }