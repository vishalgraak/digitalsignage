package com.digital.signage.retrofit;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface ApiInterface {
    @GET("movie/top_rated")
    Call<LoginModel> getLoginDetails(/*@Query("api_key") String apiKey*/);
    @FormUrlEncoded
    @POST("api.php")
    Call<ResponseModel> getSignageData(@Field("method") String method, @Field("signage_id") String signageId, @Field("device_id") String deviceId);
}