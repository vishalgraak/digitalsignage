package com.digital.signage.Notifications

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.os.Parcelable
import android.support.v4.content.WakefulBroadcastReceiver
import android.util.Log
import com.digital.signage.Activity.ImagePagerView
import com.digital.signage.Activity.ImageVideoView
import com.digital.signage.Activity.VideoView
import com.digital.signage.retrofit.DataModel
import com.google.gson.Gson

/**
 * Created by Vishal on 23/05/2018.
 */
 class AlarmService: BroadcastReceiver() {
    private var mPrefs: SharedPreferences? = null
    private var mImageModel:ArrayList<DataModel>?=null
    private var mVideoModel:ArrayList<DataModel>?=null
    var mIntent: Intent?=null
    override fun onReceive(context: Context?, intent: Intent?) {
        mPrefs = context!!.getSharedPreferences("DigitalSignage", Context.MODE_PRIVATE);
       // val mDataModel = intent!!.getParcelableExtra<DataModel>("dataList")
        val mPlayString = intent!!.getStringExtra("playTime")
        val mTopText = intent!!.getStringExtra("TopText")
        val mText = intent!!.getStringExtra("text")
        val mSpeed = intent!!.getStringExtra("speed")
        val mType = intent!!.getStringExtra("type")
        var mDataModelNew=DataModel()
        Log.e("alarmservice called","")
        mDataModelNew.text=mText
        mDataModelNew.speed=mSpeed
        mDataModelNew.type=mType
        val gson = Gson()
        val mDataModelstr = gson.toJson(mDataModelNew)
        val mEditor = mPrefs!!.edit()
        mEditor.putString("DataModel", mDataModelstr)
        mEditor.putString("playTime", mPlayString)
        mEditor.putString("TopText", mTopText)
        Log.e("check screen type",""+mType)
        var screenType:String = mType
        if(screenType.equals("img")){
            mImageModel= intent!!.getParcelableArrayListExtra<Parcelable>("imageList") as ArrayList<DataModel>;
           var mImageModelstr = gson.toJson(mImageModel)
            mEditor.putString("ImageModel", mImageModelstr)
            mEditor.apply()
            mIntent = Intent(context, ImagePagerView::class.java)
            mIntent!!.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            mIntent!!.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
            context.startActivity(mIntent)
        }else if(screenType.equals("video")){

            mVideoModel= intent!!.getParcelableArrayListExtra<Parcelable>("videoList") as ArrayList<DataModel>;
            var mVideoModelstr = gson.toJson(mVideoModel)
            mEditor.putString("VIdeoModel", mVideoModelstr)
            mEditor.apply()
            mIntent = Intent(context, VideoView::class.java)
            mIntent!!.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            mIntent!!.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
            context.startActivity(mIntent)
        }else{
            mImageModel= intent!!.getParcelableArrayListExtra<Parcelable>("imageList") as ArrayList<DataModel>;
            mVideoModel= intent!!.getParcelableArrayListExtra<Parcelable>("videoList") as ArrayList<DataModel>;
            var mVideoModelstr = gson.toJson(mVideoModel)
            var mImageModelstr = gson.toJson(mImageModel)
            mEditor.putString("ImageModel", mImageModelstr)
            mEditor.putString("VideoModel", mVideoModelstr)
            mEditor.apply()
            mIntent = Intent(context, ImageVideoView::class.java)
            mIntent!!.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            mIntent!!.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
            context.startActivity(mIntent)
        }
    }

}