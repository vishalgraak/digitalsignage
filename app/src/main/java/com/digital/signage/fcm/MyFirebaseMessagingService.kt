package com.digital.signage.fcm

import android.annotation.SuppressLint
import android.app.AlarmManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.graphics.Bitmap
import android.media.RingtoneManager
import android.net.Uri
import android.os.AsyncTask
import android.os.Build
import android.os.Environment
import android.support.v4.app.NotificationCompat
import android.support.v4.content.ContextCompat
import android.support.v4.os.EnvironmentCompat
import android.text.TextUtils
import android.util.Log
import android.view.View
import android.widget.Toast
import com.digital.signage.Activity.ImagePagerView
import com.digital.signage.Activity.ImageVideoView
import com.digital.signage.Activity.VideoView
import com.digital.signage.Notifications.AlarmService
import com.digital.signage.Notifications.ScheduleAlarm
import com.digital.signage.retrofit.ApiClient
import com.digital.signage.retrofit.ApiInterface

import com.digital.signage.retrofit.DataModel
import com.digital.signage.retrofit.ResponseModel
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import com.google.gson.Gson


import org.json.JSONException
import org.json.JSONObject

import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.text.SimpleDateFormat
import java.util.*
import kotlin.jvm.internal.Intrinsics

/**
 * Created by Bhavya on 1/31/2017.
 */

class MyFirebaseMessagingService : FirebaseMessagingService() {
    private val call: Call<ResponseModel>? = null
    private var checkStartScreen = false
    private var curTimeBool = false

    private var day: String? = null

    private var mEndTime: String? = null

    private val hour: Int? = null
    private val intent: Intent? = null

    private var mAnothDayModel: DataModel? = null
    private var mCurrTimeModel: DataModel? = null
    private var mCurrDayModel: DataModel? = null
    private var mPrefs: SharedPreferences? = null

    private val minute: Int? = null
    private var nexTimeBool = java.lang.Boolean.valueOf(false)
    private var perTimeBool = java.lang.Boolean.valueOf(false)
    private var refreshedToken: String? = null
    private var startScreen: String? = null
    var scheduleAlarm:ScheduleAlarm? = null
    override fun onMessageReceived(remoteMessage: RemoteMessage?) {
        Log.e(TAG, "From: " + remoteMessage!!.from!!)


        if (remoteMessage.data.size > 0) {
            Log.d("PayLoad", "Message data payload: " + remoteMessage.data)
            sendNotification(remoteMessage)
        }
        if (remoteMessage != null) {
            sendNotification(remoteMessage)
        }

    }

    private fun sendNotification(remoteMessage: RemoteMessage) {

        checkStartScreen = false
        Log.e("Check totalNotification", "data payload: " + remoteMessage.data)
        try {
            mPrefs = getSharedPreferences("DigitalSignage", Context.MODE_PRIVATE)
            // val msgBody = remoteMessage.data[TtmlNode.TAG_BODY] as String
            val msgTitle = remoteMessage.data["title"] as String

            val signageId = mPrefs!!.getString("SignageId", "")

            val sharedPreferences2 = this.mPrefs

            val regId = sharedPreferences2!!.getString("regId", "")

            RingtoneManager.getRingtone(applicationContext, RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION)).play()
            getData(signageId, regId);

        } catch (e: Exception) {
            e.printStackTrace()


        }

    }

    private fun getData(signageId: String, regId: String) {

        val apiService = ApiClient.getClient()!!.create(ApiInterface::class.java!!)
        try {
            val call = apiService.getSignageData("get_add_data", signageId, regId)
            call.enqueue(object : Callback<ResponseModel> {
                override fun onResponse(call: Call<ResponseModel>, response: Response<ResponseModel>) {

                    var responseData: ResponseModel = response.body()
                    var status = responseData.status
                    var message = responseData.message
                    var signageActive = responseData.active
                    if (status == "true") {
                        if (signageActive == "true") {
                            var mDataList = responseData.dataList
                            AsyncAlarmScheduler().execute(mDataList)
                        } else if (signageActive == "false") {
                            var mEditor = mPrefs!!.edit()
                            mEditor.putString("SignageId", signageId)
                            mEditor.putString("active", "false")
                            mEditor.apply()
                            var mIntent = Intent(this@MyFirebaseMessagingService, ImagePagerView::class.java)
                            mIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                            mIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                            startActivity(mIntent)
                        }

                    } else {
                        Toast.makeText(this@MyFirebaseMessagingService, message, Toast.LENGTH_SHORT).show()
                        if (signageActive.equals("false")) {
                            var mEditor = mPrefs!!.edit()
                            mEditor.putString("SignageId", signageId)
                            mEditor.putString("active", "false")
                            mEditor.apply()
                            var mIntent = Intent(this@MyFirebaseMessagingService, ImagePagerView::class.java)
                            mIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                            mIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                            startActivity(mIntent)
                        }
                    }
                }

                override fun onFailure(call: Call<ResponseModel>, t: Throwable) {
                    // Log error here since request failed

                }
            })
        } catch (e: IllegalStateException) {
            e.printStackTrace()
        }
    }

    inner class AsyncAlarmScheduler : AsyncTask<ArrayList<DataModel>, String, String>() {

        override fun onPreExecute() {
            super.onPreExecute()
            scheduleAlarm = ScheduleAlarm(this@MyFirebaseMessagingService)
        }

        override fun doInBackground(vararg params: ArrayList<DataModel>?):String {
            //To change body of created functions use File | Settings | File Templates.
            checkStartScreen = false
            setScheduleAlarm(params[0]!!)

            return ""
        }

        override fun onPostExecute(result: String?) {
            super.onPostExecute(result)
            setScheduleScreen()
        }

    }


    private fun setScheduleScreen() {
        try {

          //  cancelAllPendingIntent()


            //scheduleAlarm.scheduleAlarm(this@MyFirebaseMessagingService, 15, 0, mCurrTimeModel, "sun", "Stop")
            //scheduleAlarm.scheduleAlarm(this@MyFirebaseMessagingService, 15, 1, mCurrTimeModel, "sun", "Start")
            if (checkStartScreen) {
               // checkStartScreen= false
                if (curTimeBool) {
                    Log.e("check current screen",""+mCurrTimeModel!!.topText)
                    startScreen = "Start"
                    showScreen(mCurrTimeModel!!)
                    curTimeBool=false
                } else {
                    Log.e("check other screen",""+mCurrDayModel!!.topText)
                    startScreen = "Stop"
                    showScreen(mCurrDayModel!!)
                }

            } else {
                Log.e("check other day screen",""+mAnothDayModel!!.topText)
                startScreen = "Stop"
                showScreen(mAnothDayModel!!)
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }
 private fun setScheduleAlarm(mDataList: ArrayList<DataModel>){

     for (a: Int in 0 until mDataList.size) {
         val mTimingList = mDataList[a].timing
         if (mTimingList.isNotEmpty()) {
             for (i in 0 until mTimingList.size) {
                 day = mTimingList[i].day
                 var time = mTimingList[i].time
                 if (time == "-1") {


                     scheduleAlarm!!.scheduleAlarm(this@MyFirebaseMessagingService, 0, 0, mDataList[a], day, "Start")
                 } else {
                     val mScheduleList = mTimingList[i].timeList

                     for (c in 0 until mScheduleList.size) {
                         val str: String
                         val scheduleTime = mScheduleList[c].schedule

                         val strSplit = scheduleTime.split("-")
                         val startTime = strSplit[0].trim()
                         val startHour = startTime.split(":")[0].trim()
                         val startMinutes = startTime.split(":")[1].trim()
                         val endTime = strSplit[1].trim()
                         val endHour = endTime.split(":")[0].trim()
                         val endMinutes = endTime.split(":")[1].trim()

                         scheduleAlarm!!.scheduleAlarm(this@MyFirebaseMessagingService, startHour.toInt(), startMinutes.toInt(), mDataList[a], day, "Start")

                         /*if (mScheduleList.size == 1) {
    scheduleAlarm.scheduleAlarm(this@MyFirebaseMessagingService, startHour.toInt(), startMinutes.toInt(), mDataList[a], day, "Start")
    scheduleAlarm.scheduleAlarm(this@MyFirebaseMessagingService, endHour.toInt(), endMinutes.toInt(), mDataList[a], day, "Stop")
} else {

    if (startTime == mEndTime) {
        scheduleAlarm.scheduleAlarm(this@MyFirebaseMessagingService, startHour.toInt(), startMinutes.toInt(), mDataList[a], day, "Start")

    } else {
        scheduleAlarm.scheduleAlarm(this@MyFirebaseMessagingService, startHour.toInt(), startMinutes.toInt(), mDataList[a], day, "Start")
        scheduleAlarm.scheduleAlarm(this@MyFirebaseMessagingService, endHour.toInt(), endMinutes.toInt(), mDataList[a], day, "Stop")
    }
}*/
                         mEndTime = endTime
                         val dayOfTheWeek = SimpleDateFormat("EEE").format(Date())
                         var currentDay = day!!.substring(0, 1).toUpperCase() + day!!.substring(1, day!!.length).toLowerCase()
                         if (dayOfTheWeek == currentDay) {
                             checkStartScreen = true
                             if (schedulingFirstScreen(startTime, endTime)) {
                                 Log.e("current", ""+scheduleTime)
                                 mCurrTimeModel = mDataList[a]
                             } else {
                                 Log.e("not current", ""+scheduleTime)
                                 mCurrDayModel = mDataList[a]
                             }
                         } else {

                             mAnothDayModel = mDataList[a]
                         }

                         //  Log.e("check day::",""+day)
                     }
                 }


             }
         }
     }
 }
    private fun showScreen(mDataModel: DataModel) {
        val gson = Gson()
        val mDataModelStr = gson.toJson(mDataModel)
        val mImageModelStr = gson.toJson(mDataModel.imagesList)
        val mVideoModelStr = gson.toJson(mDataModel!!.videos)


        val mEditor = mPrefs!!.edit()
        mEditor.putString("DataModel", mDataModelStr)
        mEditor.putString("ImageModel", mImageModelStr)
        mEditor.putString("VideoModel", mVideoModelStr)
        mEditor.putString("active", "true")
        mEditor.putString("playTime", startScreen)
        mEditor.putString("TopText", mDataModel.topText)

        try {
            val downloadDir = getExternalStorageDirectories()

            for (i in 0 until downloadDir.size) {
                mEditor.putString("downloadDirectory", downloadDir[i])
            }
            mEditor.apply()
        } catch (e: Exception) {
            e.printStackTrace()
        }
        mEditor.apply()


        val screenType = mDataModel.type

        openActivity(screenType)
    }

    private fun openActivity(screenType: String?) {

        val mIntent: Intent
        if (screenType == "img") {
            mIntent = Intent(this, ImagePagerView::class.java)
            mIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            mIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
            startActivity(mIntent)

        } else if (screenType == "video") {
            mIntent = Intent(this, VideoView::class.java)
            mIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            mIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
            startActivity(mIntent)

        } else {
            mIntent = Intent(this, ImageVideoView::class.java)
            mIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            mIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
            startActivity(mIntent)

        }
    }

    @SuppressLint("LongLogTag")
    private fun schedulingFirstScreen(startTime: String, endTime: String): Boolean {
        val cal = Calendar.getInstance()
        val mFormat = SimpleDateFormat("HH:mm")
        val d1 = mFormat.parse(startTime)

        cal.time = d1
        val mStart = cal.timeInMillis
        cal.clear()
        if(endTime.trim() == "00:00"){

            cal.time = mFormat.parse("23:59")
        }else{
            cal.time = mFormat.parse(endTime)
        }
        //cal.time = mFormat.parse(endTime)
        val mEnd = cal.timeInMillis
        val result = mFormat.format(Calendar.getInstance().time)
        cal.clear()
        cal.time = mFormat.parse(result)
        val mReal = cal.timeInMillis
        val j = mEnd - 1
        if (mStart <= mReal && j >= mReal) {
            Log.e("Current Start Time and End Time", "$startTime=$endTime=$result")
            curTimeBool = true
            return true
        } else if (mReal > mStart) {
            Log.e("Previous Start Time and End Time", "$startTime=$endTime=$result")
            perTimeBool = true
            return false
        } else {
            Log.e("next Start Time and End Time", "" + startTime + '=' + endTime)
            nexTimeBool = true
            return false

        }
    }

    companion object {
        private val TAG = MyFirebaseMessagingService::class.java.simpleName
    }

    private fun cancelAllPendingIntent() {
        var i = 101

        val requestCode = mPrefs!!.getInt("PendingIntent", 101)
        while (i < requestCode) {
            Log.e("{{{{{request Code}}}}}", "" + i)
            val pendingIntent = PendingIntent.getBroadcast(this, i, Intent(applicationContext, AlarmService::class.java), PendingIntent.FLAG_UPDATE_CURRENT)
            val am = getSystemService(Context.ALARM_SERVICE) as AlarmManager

            am.cancel(pendingIntent)
            i++
        }
    }

    /* returns external storage paths (directory of external memory card) as array of Strings */
    fun getExternalStorageDirectories(): Array<String?> {
        val results = ArrayList<String>()
        try {


            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) { //Method 1 for KitKat & above
                val externalDirs = ContextCompat.getExternalFilesDirs(this@MyFirebaseMessagingService, null)

                for (file in externalDirs) {
                    val path = file.path.split("/Android")[0]

                    var addPath = false

                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                        addPath = Environment.isExternalStorageRemovable(file)
                    } else {
                        addPath = Environment.MEDIA_MOUNTED.equals(EnvironmentCompat.getStorageState(file))
                    }

                    if (addPath) {
                        results.add(path)
                    }
                }
            }

            if (results.isEmpty()) { //Method 2 for all versions
                // better variation of: http://stackoverflow.com/a/40123073/5002496
                var output = ""
                try {
                    val process = ProcessBuilder().command("mount | grep /dev/block/vold")
                            .redirectErrorStream(true).start()
                    process.waitFor()
                    val `is` = process.inputStream
                    val buffer = ByteArray(1024)
                    while (`is`.read(buffer) !== -1) {
                        output = output + String(buffer)
                    }
                    `is`.close()
                } catch (e: Exception) {
                    e.printStackTrace()
                }

                if (!output.trim { it <= ' ' }.isEmpty()) {
                    val devicePoints = output.split("\n".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
                    for (voldPoint in devicePoints) {
                        results.add(voldPoint.split(" ".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()[2])
                    }
                }
            }

            //Below few lines is to remove paths which may not be external memory card, like OTG (feel free to comment them out)
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                var i = 0
                while (i < results.size) {
                    if (!results.get(i).toLowerCase().matches(".*[0-9a-f]{4}[-][0-9a-f]{4}".toRegex())) {
                        Log.d("TAG", results.get(i) + " might not be extSDcard")
                        results.removeAt(i--)
                    }
                    i++
                }
            } else {
                var i = 0
                while (i < results.size) {
                    if (!results.get(i).toLowerCase().contains("ext") && !results.get(i).toLowerCase().contains("sdcard")) {
                        Log.d("TAG2", results.get(i) + " might not be extSDcard")
                        results.removeAt(i--)
                    }
                    i++
                }
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
        val storageDirectories = arrayOfNulls<String>(results.size)
        for (i in results.indices) storageDirectories[i] = results.get(i)

        return storageDirectories
    }

}