package com.digital.signage.Notifications;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.SystemClock;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.digital.signage.retrofit.DataModel;

import java.util.Calendar;


/**
 * Created by Vishal Graak on 23/05/2018.
 */

public class ScheduleAlarm extends AppCompatActivity {


    Context context;

    //public static int
    //categoryNumber= 2:water;

    static SharedPreferences pref1;
    private int requestCode = 101;

    public ScheduleAlarm(Context context) {
        this.context = context;

    }


    public static void setAlarm(AlarmManager am, long time, PendingIntent pendingIntent, int requestCode) {
        if (Build.VERSION.SDK_INT >= 19)
            setAlarmNewAPI(am, time, pendingIntent, requestCode);
        else
            setAlarmOldAPI(am, time, pendingIntent, requestCode);
    }

    @TargetApi(19)
    public static void setAlarmNewAPI(AlarmManager am, long time, PendingIntent pendingIntent, int requestCode) {
        //Log.d("alarmtest", "setEXACT - " + Long.toString(time) + " -- " + Integer.toString(requestCode));
       am.setExact(AlarmManager.RTC_WAKEUP, time, pendingIntent);
        am .setRepeating(AlarmManager.RTC_WAKEUP,
                time ,
                7*AlarmManager.INTERVAL_DAY, pendingIntent);
    }

    public static void setAlarmOldAPI(AlarmManager am, long time, PendingIntent pendingIntent, int requestCode) {
      //  Log.d("alarmtest", "setOLD - " + Long.toString(time) + " -- " + Integer.toString(requestCode));
        am.set(AlarmManager.RTC_WAKEUP, time, pendingIntent);
        am .setRepeating(AlarmManager.RTC_WAKEUP,
                time ,
                7*AlarmManager.INTERVAL_DAY, pendingIntent);
    }


    @SuppressLint("LongLogTag")
    public void scheduleAlarm(Context context, int hour, int minute, DataModel dataModel, String day, String playTime) {
        Calendar calendar = Calendar.getInstance();


        // we can set time by open date and time picker dialog
       // Log.e("check screen type schedule",""+dataModel.type);
        Intent intent1 = new Intent(context, AlarmService.class);
        intent1.putExtra("playTime", playTime);
        intent1.putExtra("dataList", dataModel);
        intent1.putExtra("text", dataModel.text);
        intent1.putExtra("speed", dataModel.speed);
        intent1.putExtra("type", dataModel.type);
        intent1.putExtra("TopText", dataModel.topText);
      //  Log.e("check top_text_schedule", "" + dataModel.topText);
        if (dataModel.type.equals("img")) {
            intent1.putParcelableArrayListExtra("imageList", dataModel.imagesList);
        } else if (dataModel.type.equals("video")) {
            intent1.putParcelableArrayListExtra("videoList", dataModel.videos);
        } else {
            intent1.putParcelableArrayListExtra("videoList", dataModel.videos);
            intent1.putParcelableArrayListExtra("imageList", dataModel.imagesList);
        }
      //  Log.d("Other than Medicines check Time", "" + hour + ",," + minute);
        calendar.setTimeInMillis(System.currentTimeMillis());
        calendar.set(Calendar.DAY_OF_WEEK, dayOfWeek(day));
        calendar.set(Calendar.HOUR_OF_DAY, hour);
        calendar.set(Calendar.MINUTE, minute);
        calendar.set(Calendar.SECOND, 0);

Log.e("check alarm time:",dayOfWeek(day) +" "+hour+" "+ minute);
     //   calendar.setTimeInMillis(calendar.getTimeInMillis());// Okay, then tomorrow ...

        if (System.currentTimeMillis() > calendar.getTimeInMillis()) {
            calendar.setTimeInMillis(calendar.getTimeInMillis() + 7 * 24 * 60 * 60 * 1000);// Okay, then tomorrow ...
        }
Log.e("code::",""+requestCode+" "+calendar.getTime());
        PendingIntent pendingIntent = PendingIntent.getBroadcast(
                context, requestCode, intent1,
                PendingIntent.FLAG_UPDATE_CURRENT);

        AlarmManager am = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);

        SharedPreferences.Editor editor = context.getSharedPreferences("DigitalSignage", Context.MODE_PRIVATE).edit();
        editor.putInt("pendingIntent", requestCode);
        editor.apply();
    //    Log.e("Other than MedicTime", "" + requestCode);
        requestCode=requestCode+1;
        setAlarm(am, calendar.getTimeInMillis(), pendingIntent, 0);
    }

    public int dayOfWeek(String strDay) {
        int day=0;
        if (strDay.equals("sun")) {
            return 1;
        }
        if (strDay.equals("mon")) {
            return 2;
        }
        if (strDay.equals("tue")) {
            return 3;
        }
        if (strDay.equals("wed")) {
            return 4;
        }
        if (strDay.equals("thu")) {
            return 5;
        }
        if (strDay.equals("fri")) {
            return 6;
        }
        if (strDay.equals("sat")) {
            return 7;
        }
       return 1;
    }
}


